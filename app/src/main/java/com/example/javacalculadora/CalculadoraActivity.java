package com.example.javacalculadora;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSuma;
    private Button btnResta;
    private Button btnMulti;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;

    private EditText txtnum1;
    private EditText txtnum2;
    private TextView lblResultado;
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora03);

        iniciarComponentes();

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSuma();
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnResta();
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMulti();
            }
        });

        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void iniciarComponentes() {
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnDividir = findViewById(R.id.btnDividir);
        btnMulti = findViewById(R.id.btnMulti);
        txtnum1 = findViewById(R.id.txtnum1);
        txtnum2 = findViewById(R.id.txtnum2);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnregresar);

        lblResultado = findViewById(R.id.lblresultado);
        calculadora = new Calculadora(0, 0);
    }

    private void btnSuma() {
        calculadora.setNum1(Integer.parseInt(txtnum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtnum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.suma()));
    }

    private void btnResta() {
        calculadora.setNum1(Integer.parseInt(txtnum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtnum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.resta()));
    }

    private void btnDividir() {
        calculadora.setNum1(Integer.parseInt(txtnum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtnum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.division()));
    }

    private void btnMulti() {
        calculadora.setNum1(Integer.parseInt(txtnum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtnum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.multiplicacion()));
    }

    private void limpiar() {
        txtnum1.setText("");
        txtnum2.setText("");
        lblResultado.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Deseas regresar?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", null);
        confirmar.show();
    }
}
