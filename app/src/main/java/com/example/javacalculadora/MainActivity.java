package com.example.javacalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.javacalculadora.CalculadoraActivity;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnLogin;
    private Button btncerrar;
    private EditText txtUsuario;
    private EditText txtContraseña;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        btncerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnLogin = findViewById(R.id.btnLogin);
        btncerrar = findViewById(R.id.btncerrar);
        txtContraseña = findViewById(R.id.txtContraseña);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void login() {
        String strUsuario = getResources().getString(R.string.usuario);
        String strContra = getResources().getString(R.string.contraseña);

        if (txtUsuario.getText().toString().equals(strUsuario) && txtContraseña.getText().toString().equals(strContra)) {
            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtra("usuario", strUsuario);
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this, "Usuario o contraseña no válida", Toast.LENGTH_LONG).show();
        }
    }

    private void salir() {
        finish();
    }
}
